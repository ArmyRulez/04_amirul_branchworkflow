﻿using System;

namespace _Amirul_BranchWorkflow
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int num;
            int sum = 0;
            Console.WriteLine("Please enter a number : ");
            num = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= num; i++)
            {
                Console.WriteLine(i);
                sum += i;
            }

            Console.WriteLine(sum);
            Console.WriteLine("The sum of the numbers is : " + sum);
        }
    }
}
